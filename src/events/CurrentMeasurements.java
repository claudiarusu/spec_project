package events;

public class CurrentMeasurements {
	
	private Integer Id;
	private String Timestamp;
	private Double Current;
	private Double Voltage;
	private Double Frequency;
	
	public CurrentMeasurements(Integer id, String timestamp, Double current, Double voltage, Double frequency) {
		super();
		Id = id;
		Timestamp = timestamp;
		Current = current;
		Voltage = voltage;
		Frequency = frequency;
	}

	public Integer getId() {
		return Id;
	}

	public String getTimestamp() {
		return Timestamp;
	}
	
	public Double getCurrent() {
		return Current;
	}
	
	public Double getVoltage() {
		return Voltage;
	}
	
	public Double getFrequency() {
		return Frequency;
	}

	@Override
	public String toString() {
		return "CurrentMeasurements: Id:" + Id + ", Timestamp:" + Timestamp + ", Current:" + Current + ", Voltage:"
				+ Voltage + ", Frequency:" + Frequency + ".";
	}
}
