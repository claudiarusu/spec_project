package project;

import java.io.BufferedReader;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import com.espertech.esper.client.Configuration;
import com.espertech.esper.client.EPServiceProvider;
import com.espertech.esper.client.EPServiceProviderManager;
import com.espertech.esper.client.EPStatement;
import com.espertech.esper.client.EventBean;
import com.espertech.esper.client.UpdateListener;

import events.CurrentMeasurements;

/**
 * 
 * At the end of the main method, a stream of events current measurements is
 * generated from a CSV file. The application reads, every second, an observation
 * from the CSV and push it into the CEP engine.
 * 
 * The main fields of the current measurements are: timestamp, current, voltage and frequency
 * 
 * Create an event processing network which is able to detect the following:
 * 
 * I.   Detect the moments of time when current increase rate is greater than 0.4 amps for more than:
 * 	    1. 3 seconds
 * 	    2. 6 seconds
 * II.  Detect the moments of time when the voltage value decreases by 10%;
 * III. Detect the moments of time when the current increase rate is greater than 0.4 amps and is followed by a sudden voltage drop;
 * IV.  Signal, once a day, how many times the situation in point III. has been identified.
 * 
 */

public class Main {

	public static void main(String[] args) throws InterruptedException {

		Configuration configuration = new Configuration();
		configuration.addEventTypeAutoName("events");
		EPServiceProvider epService = EPServiceProviderManager.getDefaultProvider(configuration);
		
		String expression_data_printing = "select * from CurrentMeasurements";
		EPStatement statement_data_printing = epService.getEPAdministrator().createEPL(expression_data_printing);

		statement_data_printing.addListener(new UpdateListener() {

			@Override
			public void update(EventBean[] arg0, EventBean[] arg1) {
				for (int i = 0; i < arg0.length; i++) {
					EventBean event = arg0[i];

					//System.out.println("CurrentMeasurements: Id: " +event.get("id")+ ", Timestamp: " + event.get("timestamp") +
					//						", Current: " + (Double)event.get("current") + ", Voltage: " + (Double)event.get("voltage")  + 
					//						", Frequency: " + (Double)event.get("frequency") + ";" );
				}
			}
		});
			
		
		// Resolve from here:
				
		//-------------------------------------------------------------------------------------------------------------------------
		// I. Detect the moments of time when current increase rate is greater than 0.4 amps for more than:
		//-------------------------------------------------------------------------------------------------------------------------
		
		// Calculate the slope on time and current between 2 events
		String expression_current_increase_slopes = "insert into Slopes select slope, XAverage as time from CurrentMeasurements.win:time(2 sec).stat:linest(id, current)";
		EPStatement statement_current_increase_slopes = epService.getEPAdministrator().createEPL(expression_current_increase_slopes);
		
		// 1. Create a window of 3 seconds and get the minimum slope and the average of time
		String expression_current_increase_time_win_3s = "insert into MinSlope_3s select min(slope) as minSlope, avg(time) as time from Slopes.win:time(3 sec)";
		EPStatement statement_current_increase_time_win_3s = epService.getEPAdministrator().createEPL(expression_current_increase_time_win_3s);
		
		// 2. Create a window of 5 seconds and get the minimum slope and the average of time
	    String expression_current_increase_time_win_6s = "insert into MinSlope_6s select min(slope) as minSlope, last(time) as time from Slopes.win:time(6 sec)";
		EPStatement statement_current_increase_time_win_6s = epService.getEPAdministrator().createEPL(expression_current_increase_time_win_6s);
		
		// Select only the slopes which are bigger then 0.4 
		String expression_current_increase_selective_slope = "select * from MinSlope_6s(minSlope > 0.4)";
		EPStatement statement_current_increase_selective_slope = epService.getEPAdministrator().createEPL(expression_current_increase_selective_slope);
		
		statement_current_increase_selective_slope.addListener(new UpdateListener() {
			@Override
			public void update(EventBean[] arg0, EventBean[] arg1) {
			
				for(int i=0; i<arg0.length; i++) {
					EventBean event = arg0[i];

					System.out.println("Current increment - Time: " + event.get("time") +" - Slope: " + event.get("minSlope"));
				}
			}
		});
		
		//------------------------------------------------------------------------------------------------------------------------
		// II.  Detect the moments of time when the voltage value decreases by 10%:
		//------------------------------------------------------------------------------------------------------------------------
		
		// Get the events when it is a normal voltage (greater than or equal to 5 V)
		String expression_normal_voltage = "insert into NormalVoltage select id as normalVoltageId, voltage as normalVoltage from CurrentMeasurements(voltage >= 5)";
		EPStatement statement_normal_voltage = epService.getEPAdministrator().createEPL(expression_normal_voltage);

		// Get the events when it is a voltage drop 
		String expression_voltage_drop = "insert into VoltageDrop select b.id as voltageDropId from pattern[every(a=NormalVoltage -> b=CurrentMeasurements(voltage = normalVoltage - (normalVoltage * 0.1)))]";
		EPStatement statement_voltage_drop = epService.getEPAdministrator().createEPL(expression_voltage_drop);
		
		statement_voltage_drop.addListener(new UpdateListener() {
			@Override
			public void update(EventBean[] arg0, EventBean[] arg1) {
			   
				System.out.println();
				
				for(int i=0; i<arg0.length; i++) {
					EventBean event = arg0[i];

					System.out.println("Voltage drop detected - Time: " + event.get("voltageDropId"));
				}
			}
		});
		
		//---------------------------------------------------------------------------------------------------------------------------------------
		// III. Detect the moments of time when the current increase rate is greater than 0.4 amps and is followed by a sudden voltage drop:
		//---------------------------------------------------------------------------------------------------------------------------------------
		
		String expression_warning = "insert into Warning select d.voltageDropId as warningId from pattern[every(c=MinSlope_3s(minSlope > 0.4) -> (d=VoltageDrop and not (timer:interval(2 sec))))]";
		EPStatement statement_warning = epService.getEPAdministrator().createEPL(expression_warning);
		
		statement_warning.addListener(new UpdateListener() {
		@Override
		public void update(EventBean[] arg0, EventBean[] arg1) {
		
			System.out.println();
			
			for(int i=0; i<arg0.length; i++) {
				EventBean event = arg0[i];

				System.out.println("Warning detected - Time: " + event.get("warningId"));
			}
		}
		});
		
		//-------------------------------------------------------------------------------------------------------------------------
		// IV. Signal, once a day, how many times the situation in point III. has been identified:
		//-------------------------------------------------------------------------------------------------------------------------
		
		String expression_raport = "select count(warningId) as countWarning from Warning.win:time_batch(23 sec)";
		EPStatement statement_raport = epService.getEPAdministrator().createEPL(expression_raport);
		
		statement_raport.addListener(new UpdateListener() {
			@Override
			public void update(EventBean[] arg0, EventBean[] arg1) {
			
				System.out.println();
				
				for(int i=0; i<arg0.length; i++) {
					EventBean event = arg0[i];

					System.out.println("Report - Warning times per day: " + event.get("countWarning"));
				}
			}
			});
		
		
		try {
			BufferedReader br = new BufferedReader(new FileReader("Resources/current_measurements.csv"));

			String line = br.readLine();
			while ((line = br.readLine()) != null) {

				String[] tokens = line.split(",");

				CurrentMeasurements measurement = new CurrentMeasurements(Integer.parseInt(tokens[0]), 
																		  tokens[1], 
																		  Double.parseDouble(tokens[2]), 
																		  Double.parseDouble(tokens[3]), 
																		  Double.parseDouble(tokens[4]));

				epService.getEPRuntime().sendEvent(measurement);

				Thread.sleep(1000);
			}

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println("Finish");
	
	}
}
